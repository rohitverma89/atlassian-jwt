package com.atlassian.jwt.internal.applinks;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.jwt.JwtIssuerRegistry;
import com.atlassian.jwt.applinks.JwtApplinkFinder;
import com.atlassian.sal.api.features.DarkFeatureManager;

import javax.annotation.Nonnull;

/**
 * Registry of JWT issuers backed by application links
 */
public class ApplinksJwtIssuerRegistry implements JwtIssuerRegistry
{
    private final JwtApplinkFinder jwtApplinkFinder;
    private final DarkFeatureManager darkFeatureManager;

    private static final String CONNECT_PLUGIN_SETTINGS_FEATURE_FLAG = "connect.no-applinks";
    
    public ApplinksJwtIssuerRegistry(JwtApplinkFinder jwtApplinkFinder, DarkFeatureManager darkFeatureManager)
    {
        this.jwtApplinkFinder = jwtApplinkFinder;
        this.darkFeatureManager = darkFeatureManager;
    }

    @Override
    public ApplicationLinkJwtIssuer getIssuer(@Nonnull String issuer)
    {
        if (darkFeatureManager.isFeatureEnabledForAllUsers(CONNECT_PLUGIN_SETTINGS_FEATURE_FLAG))
        {
            return null;
        }
        if (issuer == null)
        {
            return null;
        }
        ApplicationLink link = jwtApplinkFinder.find(issuer);
        return link == null ? null : new ApplicationLinkJwtIssuer(link);
    }
}
